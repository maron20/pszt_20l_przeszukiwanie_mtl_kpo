import unittest
import main


class MyTestCase(unittest.TestCase):
    def test_gen_individual(self):
        coins = [1, 2, 5]
        amount = 17
        individual = main.gen_individual(coins, amount)
        print(individual)
        self.assertEqual(sum(individual), amount, "Wrong sum")


if __name__ == '__main__':
    unittest.main()
